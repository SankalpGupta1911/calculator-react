import React, { Component } from "react";
import NumericalButton from "./NumericalButtons";

class Buttons extends Component {
  state = {
    buttonlist: [
      "Enter",
      "0",
      ".",
      "*",
      "1",
      "2",
      "3",
      "-",
      "4",
      "5",
      "6",
      "+",
      "7",
      "8",
      "9",
      "/",
      "clear",
    ],
  };

  render() {
    return (
      <div className="button-section">
        {this.state.buttonlist.map((buttonValue) => {
          return (
            <NumericalButton
              key={buttonValue}
              clickAction={this.props.onclick}
              value={buttonValue}
              isclear={buttonValue === "clear" ? true : false}
            />
          );
        })}
      </div>
    );
  }
}

export default Buttons;
