import React, { Component } from "react";
import DisplayBar from "./DisplayBar";
import Buttons from "./ButtonSection";
import "./style.css";

class Calculator extends Component {
  state = { screenDisplay: "Hello!" };

  handleButtonClick = (value) => {
    if (value === "Enter") {
      try {
        // eslint-disable-next-line
        let answer = eval(this.state.screenDisplay).toString();
        this.setState({
          screenDisplay: answer.length > 10 ? answer.substring(0, 11) : answer,
        });
      } catch (err) {
        this.setState({ screenDisplay: "Error!" });
      }
      return;
    } else if (value === "clear") {
      this.setState({ screenDisplay: "Hello!" });
      return;
    }
    if (this.state.screenDisplay === "Hello!") {
      this.setState({ screenDisplay: value.toString() });
      return;
    }
    this.setState({ screenDisplay: this.state.screenDisplay.concat(value) });
  };

  render() {
    return (
      <div className="main-div">
        <DisplayBar onscreen={this.state.screenDisplay} />
        <Buttons onclick={this.handleButtonClick} />
      </div>
    );
  }
}

export default Calculator;
