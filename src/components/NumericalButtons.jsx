import React, { Component } from "react";

class NumericalButton extends Component {
  render() {
    return (
      <button
        className="btn"
        style={{
          width: this.props.isclear ? "100%" : "10vw",
          backgroundColor: this.props.isclear ? "crimson" : "rgba(21, 21, 21, 0.8)"
        }}
        onClick={() => this.props.clickAction(this.props.value)}
      >
        {this.props.value}
      </button>
    );
  }
}

export default NumericalButton;
