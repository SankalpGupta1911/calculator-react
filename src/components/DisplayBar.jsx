import React, { Component } from "react";

class DisplayBar extends Component {
  render() {
    return <div className="display-bar">{this.props.onscreen}</div>;
  }
}

export default DisplayBar;
